extends Area2D
signal playerScored
signal enemyScored


# Declare member variables here. Examples:
export var speed = 1000


var StartingPosition 
var velocity = Vector2()
var screen_size
var ballVelocity = Vector2()

var playerNode: Area2D
var enemyNode: Area2D

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = Vector2(900,600)
	StartingPosition = screen_size / 2
	
	ballVelocity.x = rand_range(-1, 1)
	ballVelocity.y = rand_range(-1, 1)
	playerNode = get_node("../Player") as Area2D
	enemyNode = get_node("../Enemy") as Area2D
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	pass
	
	# X Axis
	if position.x < 12.5:
		ballVelocity.x *= -1
		_resetBall()
		_enemyScored()
		
	if position.x > screen_size.x - 12.5:
		ballVelocity.x *= -1
		_resetBall()
		_playerScored()
		
		
	# Y Axis
	if position.y < 12.5:
		ballVelocity.y *= -1
	if position.y > screen_size.y - 12.5:
		ballVelocity.y *= -1
		
	position = position + ballVelocity * delta * speed
	#print(enemyNode.get("score"))
	
	#Clamp position y so it doesnt leave screen (Only clamps inside _process) 
	#position.y = clamp(position.y, 0 + 50, screen_size.y - 50 ) 
	
	
	
	



func _on_Ball_area_entered(area):
	ballVelocity.x *= -1
	speed = speed + 250
	print("Collided with : " + area.get_name())
	print(speed)
	
func _resetBall():
	position = StartingPosition
	ballVelocity.x = rand_range(-1,1)
	ballVelocity.y = rand_range(-1,1)
	speed = rand_range(1000, 2000)
	


func _playerScored():
	emit_signal("playerScored")


func _enemyScored():
	emit_signal("enemyScored")
