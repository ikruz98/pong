extends Area2D
signal increaseScore(newScore)

# Declare member variables here. Examples:
#public(editor)
export(NodePath) var pathToBall

#private
var ballRef
var paddleOffset = 50
var screen_size
var speed = 5
var score

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = Vector2(900,600)
	ballRef = get_node(pathToBall)
	score = 0
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):	
	var posY = ballRef.position.y
	position.y = posY 
	position.y = clamp(position.y, 0 + paddleOffset, screen_size.y - paddleOffset )     #Clamp position y so it doesnt leave screen
	


func _on_Ball_enemyScored():
	score = score + 1
	emit_signal("increaseScore", score)
	
	
