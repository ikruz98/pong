extends Area2D
signal increaseScore(newScore)

# Declare member variables here. Examples:
# public (editor)
export var speed = 2


# private
var screen_size
var paddleOffset = 50
var score

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = Vector2(900,600)
	score = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	Get input and apply velocity
	var velocity = Vector2()			#create blank vec2 and add new input here
	if Input.is_action_pressed("player_down"):
		#Move Down (+=)
		velocity.y += 0.5
		
	if Input.is_action_pressed("player_up"):
		#Move Up (-=)
		velocity.y -= 0.5
		
	if velocity.length() > 0:
        velocity = velocity.normalized() * speed
	
	#copy velocity to nodes position and multiply by delta
	position += velocity * delta	
	
	#Clamp position y so it doesnt leave screen (Only clamps inside _process) 
	position.y = clamp(position.y, 0 + paddleOffset, screen_size.y - paddleOffset )     



func _on_Ball_playerScored():	
	score = score + 1
	emit_signal("increaseScore", score)
